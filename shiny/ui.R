# k-means only works with numerical variables,
# so don't give the user the option to select
# a categorical variable
vars <- setdiff(names(iris), "Species")

pageWithSidebar(
  headerPanel("MimmuWeb: A Microbe Immune Data Web Service"),
  sidebarPanel(
    width = 2,
    h4("Microbe Data Controls"),
    selectInput('Rank', 'Select A Rank', availableRanks),
    h4("Analytical Controls"),
    selectInput('Endpoint', 'Select An Endpoint', availableEndpoints),
    selectInput('qThresh', 'Select Q-value Threshold', availableQthresh)
  ),
  mainPanel(
    width = 10,
    h2("Compare Studies"),
    fluidRow(
      column(6, selectInput('Study1', 'Select Study X', names(availableStudies))),
      column(6, selectInput('Study2', 'Select Study Y', rev(names(availableStudies))))
    ),
    h2("Shared Findings"),
    fluidRow(
      tableOutput("mainFinding")
    ),
    # Output: Two-column Row for comparing plot, summary, and table ----
    # NOTE: each output element can only be used once!!!
    #       otherwise you lose tracing information and message in console!!!
    h2("Study Explorer"),
    fluidRow(
      column(6,
        tabsetPanel(type = "tabs",
          tabPanel("Stat", tableOutput("stat1")),
          tabPanel("Data", verbatimTextOutput("summary1")),
          tabPanel("Heatmap", plotlyOutput("heatmap1")),
          tabPanel("Download", downloadButton('download1',"Download the data")))),
      column(6,
        tabsetPanel(type = "tabs",
          tabPanel("Stat", tableOutput("stat2")),
          tabPanel("Data", verbatimTextOutput("summary2")),
          tabPanel("Heatmap", plotlyOutput("heatmap2")),
          tabPanel("Download", downloadButton('download2',"Download the data"))))
    )
  )
)


# fluidPage(
#   
#   # App title ----
#   titlePanel("MimmuWeb: A Microbe Immune Data Web Service"),
#   
#   # Sidebar layout with input and output definitions ----
#   sidebarLayout(
#     
#     # Sidebar panel for inputs ----
#     sidebarPanel(
#       
#       # Input: Select the analysis taxon rank ----
#       width = 2,
#       h4("Microbe Data Controls"),
#       selectInput('Rank', 'Select A Rank', availableRanks)),
#     #h1("Immune Data Controls"),
#     
#     # Main panel for displaying two comparing datasets ----
#     mainPanel(
#       # Input: Two-column Row for input study file ----
#       width = 10,
#       fluidRow(
#         column(6, selectInput('Study1', 'Select Study 1', names(availableStudies))),
#         column(6, selectInput('Study2', 'Select Study 2', names(availableStudies)))
#       ),
#       # Output: Two-column Row for comparing plot, summary, and table ----
#       fluidRow(
#         # Output: Tabset w/ plot, summary, and table ----
#         column(6, tabsetPanel(type = "tabs",
#                               tabPanel("Plot", plotOutput("plot")),
#                               tabPanel("Summary", verbatimTextOutput("summary")),
#                               tabPanel("Table", tableOutput("table")))),
#         column(6, tabsetPanel(type = "tabs",
#                               tabPanel("Plot", plotOutput("plot")),
#                               tabPanel("Summary", verbatimTextOutput("summary")),
#                               tabPanel("Table", tableOutput("table"))))
#       )
#     )
#   )
# )